import serial
from sound import Sound
from keyboard import Keyboard
from KeyKods import *

ser = serial.Serial(port='COM4', baudrate=9600)
ls = [ser.readline().decode()[:1] for _ in range(3)]
if ls == ['O', 'K', 'M']:
    print('Arduino connected!')

ss = Sound()

while True:
    inp = ser.readline().decode()[:-2]
    if inp == 'O':
        Keyboard.key(VK_MEDIA_PREV_TRACK)
        print('<<')
    if inp == 'K':
        Keyboard.key(VK_MEDIA_PLAY_PAUSE)
        print('||')
    if inp == 'M':
        Keyboard.key(VK_MEDIA_NEXT_TRACK)
        print('>>')
    print(inp)
    if inp[:1] == 'D':
        ss.volume_set(int(inp[1:]))
        inp = 0
