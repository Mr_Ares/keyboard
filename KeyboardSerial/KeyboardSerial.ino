#define PIN_BUTTON_1 2
#define PIN_BUTTON_2 3
#define PIN_BUTTON_3 4
#define POT A5

void setup() {
  Serial.begin(9600);
  pinMode(PIN_BUTTON_1, INPUT_PULLUP);
  pinMode(PIN_BUTTON_2, INPUT_PULLUP);
  pinMode(PIN_BUTTON_3, INPUT_PULLUP);
  pinMode(POT, INPUT);
}
int currentValue, prevValue, currentValue2, prevValue2, currentValue3, prevValue3, pot, postpot;
String vv;

void loop() {

  currentValue = digitalRead(PIN_BUTTON_1);
  if (currentValue != prevValue) {
    delay(400);
    currentValue = digitalRead(PIN_BUTTON_1);

    prevValue = currentValue;
    Serial.println('O');
  }
  currentValue2 = digitalRead(PIN_BUTTON_2);
  if (currentValue2 != prevValue2) {
    delay(400);
    currentValue2 = digitalRead(PIN_BUTTON_2);

    prevValue2 = currentValue2;
    Serial.println('K');
  }
  currentValue3 = digitalRead(PIN_BUTTON_3);
  if (currentValue3 != prevValue3) {
    delay(400);
    currentValue3 = digitalRead(PIN_BUTTON_3);

    prevValue3 = currentValue3;
    Serial.println('M');
  }



  pot = map(analogRead(POT), 0, 1023, 0, 100);

  if (pot != postpot and pot != (postpot - 1) and pot != (postpot + 1)) {

    postpot = pot;
    vv = 'D' + String(pot);
    Serial.println(vv);
  }
}
